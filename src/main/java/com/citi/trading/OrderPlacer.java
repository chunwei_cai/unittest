package com.citi.trading;

import java.util.function.Consumer;

/**
 * Represents any object that can place trade orders.
 * 
 * @author Will Provost
 */
public interface OrderPlacer {

	/**
	 * Place the given order, with a promise to call the given consumer
	 * with the trade notification.
	 * 
	 * @param A trade to be executed on the mock market
	 * @param An object that can be called later with the trade confirmation
	 */
	public void placeOrder(Trade order, Consumer<Trade> callback);
}
