package com.citi.trading;

import java.sql.Timestamp;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Encapsulates a stock trade.
 * 
 * @author Will Provost
 */
@XmlRootElement(name="trade")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class Trade {

    public enum Result {
        FILLED,
        PARTIALLY_FILLED,
        CANCELED,
        DONE_FOR_DAY,
        REJECTED
    };
    
	private int id;
    private Timestamp when;
	private String stock;
	private boolean buy;
    private int size;
    private double price;
    private Result result;

    /**
     * Trade timestamp defaults to the current system time.
     * All other fields are blank.
     */
	public Trade() {
		when = new Timestamp(System.currentTimeMillis());
	}
	
    /**
     * Trade timestamp defaults to the current system time.
     * All other fields are as supplied.
     */
	public Trade(String stock, boolean buy, int size, double price) {
		this(new Timestamp(System.currentTimeMillis()), stock, buy, size, price);
	}

    /**
     * All fields are as supplied, including the timestamp.
     */
	public Trade(Timestamp when, String stock, boolean buy, int size, double price) {
		this.when = when;
		this.stock = stock;
		this.buy = buy;
		this.size = size;
		this.price = price;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getStock() {
		return this.stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public boolean isBuy ()
    {
        return buy;
    }

    public void setBuy (boolean buy)
    {
        this.buy = buy;
    }

    @XmlTransient // we use the Date representation instead
    public Timestamp getWhen() {
		return this.when;
	}

	public void setWhen(Timestamp when) {
		this.when = when;
	}

	public Date getWhenAsDate ()
	{
	    return new Date (when.getTime ());
	}
	
	public void setWhenAsDate (Date date)
	{
	    when = new Timestamp (date.getTime ());
	}

	/**
	Helper to fix the timestamp to the current system time.
	*/
    public void setToNow ()
    {
        when = new Timestamp (System.currentTimeMillis ());
    }
    
    public Result getResult() {
    	return result;
    }
    
    public void setResult(Result result) {
    	this.result = result;
    }
    
    @Override
    public String toString() {
    	return String.format("Trade: [%s, %s %d %s @ %1.4f]",
    			when, buy ? "buy" : "sell", size, stock, price);
    }
}