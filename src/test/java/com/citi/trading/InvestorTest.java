package com.citi.trading;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

import java.util.function.Consumer;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class InvestorTest {
	private Investor investor;

	private class MockMarket implements OrderPlacer {
		@Override
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);
		}
	}
	
	@Before
	public void setup() {
		investor = new Investor(10000);
	}
	
	@Test
	public void Test() {
		
		//Test buy
		investor.buy("MRK", 100, 60);
		Assert.assertThat(investor.getCash(), equalTo(4000.0));
		Assert.assertThat(investor.getPortfolio(), hasKey("MRK"));
		
		//Test sell
		investor.sell("MRK", 100, 60);
		Assert.assertThat(investor.getCash(), equalTo(10000.0));
		//System.out.println(investor.getPortfolio());
		Assert.assertEquals(investor.getPortfolio(), IsMapContaining.hasEntry("MRK", 0));

	}

}
